import { atom } from "recoil";

export const initialTableData = {
    Mentions: [],
    Replies: [],
    Views: [],
};

const TableAtom = atom({
    key: "TABLE_ATOM",
    default: initialTableData,
});

export default TableAtom;