import moment from "moment";
import { atom } from "recoil";

export const initialDate ={
    UpdatedFrom: moment().subtract(14, 'days').format('YYYY-MM-DD'),
    UpdatedTo: moment().format('YYYY-MM-DD')
}

const DateRangeAtom = atom({
    key: "DATE_RANGE_ATOM",
    default: initialDate,
});

export default DateRangeAtom;