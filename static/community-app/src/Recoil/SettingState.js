import { atom } from "recoil";

export const initialSettings = {
    mentions: [],
    replies: [],
    views: [],
};

const SettingsAtom = atom({
    key: "SETTINGS_ATOM",
    default: initialSettings,
});

export default SettingsAtom;