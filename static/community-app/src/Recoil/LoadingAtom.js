import { atom } from "recoil";

export const initialState ={
    isLoadingMentions : true,
    isLoadingReplies: true,
    isLoadingViews: true 
}

const LoadingAtom = atom({
    key: "LOADING_ATOM",
    default: initialState,
});

export default LoadingAtom;