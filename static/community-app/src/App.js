import React from 'react';
import BodyComponent from './Components/Body/BodyComponent';
import Filter from './Components/Filter/Filter';

export default function App() {

  return (
    <div style={{ fontFamily: "ubuntu" }}>
      <Filter />
      <BodyComponent />
    </div>
  );
}
