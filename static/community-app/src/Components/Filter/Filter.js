import React, { useState } from "react";
import { DatePicker } from "@atlaskit/datetime-picker";
import styles from "./Filter.module.css";
import moment from "moment";
import SettingsIcon from '@atlaskit/icon/glyph/settings';
import SettingsModal from "../Settings/Settings";
import { LabelComp } from "../StyledComponents/Styling";
import { useRecoilState } from "recoil";
import DateRangeAtom from "../../Recoil/DateRangeAtom";

const Filter = () => {  
    const [dateRange, setDateRange] = useRecoilState(DateRangeAtom);
    const [openSettingsModal, setopenSettingsModal] = useState(false);

    const ChangeSettingsmodal = (values) => {
        setopenSettingsModal(false);
      };

    const formatDisplayLabel = (value, dateFormat) => {
      return moment(value).format(dateFormat);
    };
      
    return (
        <div className={styles.headercomp}>
          <div style={{ display: "flex" }}>
          <div className={styles.datecomponents}>
            <LabelComp>Updated from</LabelComp>
            <DatePicker
              onChange={(value) => {
                if(value === ""){
                  setDateRange(prev => ({
                    ...prev,
                    UpdatedFrom: moment().subtract(14, 'days').format('YYYY-MM-DD')
                  }))}
                  else{
                    setDateRange(prev => ({
                      ...prev,
                      UpdatedFrom: value
                  }))}
                }}
              placeholder={dateRange.UpdatedFrom}
              defaultValue={dateRange.UpdatedFrom}
              dateFormat="MM-DD-YYYY"
              formatDisplayLabel={formatDisplayLabel}
            />
          </div>
          <div className={styles.datecomponents}>
            <LabelComp>Updated until</LabelComp>
            <DatePicker
              onChange={(value) => {
                if(value === ""){
                  setDateRange(prev => ({
                    ...prev,
                    UpdatedTo: moment().format('YYYY-MM-DD')
                  }))}
                  else{
                    setDateRange(prev => ({
                      ...prev,
                      UpdatedTo: value
                  }))}
                }}
              placeholder={dateRange.UpdatedTo}
              defaultValue={dateRange.UpdatedTo}
              dateFormat="MM-DD-YYYY"
              formatDisplayLabel={formatDisplayLabel}
            />
          </div>
          </div>
          <div style={{ justifyContent: "flex-end", display: "flex" }}>
          <div className={styles.settingscomp}>
            <div onClick={() => setopenSettingsModal(true)}>
                <SettingsIcon size="large" />
            </div>
            <SettingsModal
                openModal={openSettingsModal}
                Changemodal={ChangeSettingsmodal}/>
          </div>
          </div>
        </div>
    );
  }
  
  export default Filter;