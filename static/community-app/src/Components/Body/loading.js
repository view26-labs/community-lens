import React from "react";
import LoaderSkeleton from '@view26/loader-skeleton';
import '@view26/loader-skeleton/dist/index.css';

const loading = () => {
    const rowSkeleton = [
    {
        cells: [{
          key: "a",
          content: (
            <LoaderSkeleton
            background={"#e9e9e9"}
            gradientColor="rgba(255,255,255,0.7)"
            style={{ borderRadius: "6px", margin: "10px" }}
            height="12px"
            />
            )
        }]
    },
    {
        cells: [{
          key: "a",
          content: (
            <LoaderSkeleton
            background={"#e9e9e9"}
            gradientColor="rgba(255,255,255,0.7)"
            style={{ borderRadius: "6px", margin: "10px" }}
            height="12px"
            />
            )
        }]
    },
    {
        cells: [{
          key: "a",
          content: (
            <LoaderSkeleton
            background={"#e9e9e9"}
            gradientColor="rgba(255,255,255,0.7)"
            style={{ borderRadius: "6px", margin: "10px" }}
            height="12px"
            />
            )
        }]
    },
    {
        cells: [{
          key: "a",
          content: (
            <LoaderSkeleton
            background={"#e9e9e9"}
            gradientColor="rgba(255,255,255,0.7)"
            style={{ borderRadius: "6px", margin: "10px" }}
            height="12px"
            />
            )
        }]
    },
    {
        cells: [{
          key: "a",
          content: (
            <LoaderSkeleton
            background={"#e9e9e9"}
            gradientColor="rgba(255,255,255,0.7)"
            style={{ borderRadius: "6px", margin: "10px" }}
            height="12px"
            />
            )
        }]
    }
  ];

  return rowSkeleton;
}

export default loading;