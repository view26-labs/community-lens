import React from "react";
import styles from "./Body.module.css";
import MentionsTable from "./MentionsTable";
import MostRepliedTable from "./MostRepliedTable";
import MostViewedTable from "./MostViewedTable";

const BodyComponent = () => {
  return (
    <div className={styles.headercomp}>
      <MentionsTable />
      <MostViewedTable />
      <MostRepliedTable />
    </div>
  );
}

export default BodyComponent;