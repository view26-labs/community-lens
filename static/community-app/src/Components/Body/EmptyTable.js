import React, { useState } from 'react';
import TableIcon from '@atlaskit/icon/glyph/table';
import Button from '@atlaskit/button';
import SettingsModal from '../Settings/Settings';
import styles from "./Body.module.css";

const EmptyTable = () => {
    const [openSettingsModal, setopenSettingsModal] = useState(false);

    const ChangeSettingsmodal = (values) => {
        setopenSettingsModal(false);
      };

    return(
        <div>
            <div className={styles.tablecomp}>
                <TableIcon size="xlarge" />
            </div>
            <div className={styles.fontcomp}>
                <h3>No data found</h3>
                <p>Try changing the search criteria</p>
                <br />
            </div>
            <div>
                <Button onClick={() => setopenSettingsModal(true)} appearance="primary">
                    Go to Settings
                </Button>
                <SettingsModal
                    openModal={openSettingsModal}
                    Changemodal={ChangeSettingsmodal} />
            </div>
        </div>
    )
};

export default EmptyTable;