import React, { useState } from "react";
import { useRecoilValue } from "recoil";
import SettingsAtom from "../../Recoil/SettingState";
import useCreateTable from "./useCreateTable";
import LoadingAtom from "../../Recoil/LoadingAtom";
import Table from "./Table";
import TableAtom from "../../Recoil/TableAtom";

const MentionsTable = () => {
  const mentions = useRecoilValue(TableAtom);
  const settingState = useRecoilValue(SettingsAtom);
  const loadState = useRecoilValue(LoadingAtom);
  const rows = useCreateTable(mentions.Mentions, settingState.mentions)
  const [rowNumber, setRowNumber] = useState(0);

  return (
    <Table heading="Mentions" subHeading="track topics of interest" tagArray={settingState.mentions} rows={rows} loadingStateVal={loadState.isLoadingMentions} rowNumber={rowNumber} setRowNumber={setRowNumber} />
  );
}

export default MentionsTable;
