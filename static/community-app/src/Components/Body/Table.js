import React, { Fragment } from "react";
import { SimpleTag as Tag } from '@atlaskit/tag';
import loading from "./loading";
import EmptyTable from "./EmptyTable";
import DynamicTable from "@atlaskit/dynamic-table";
import styled from "styled-components";

const Wrapper = styled.div`
  max-width: 450px;
  padding: 15px;
  margin: 10px 10px;
`;

const Table = ({ heading, subHeading, tagArray, rows, loadingStateVal, rowNumber, setRowNumber=()=>{} }) => {

    function extendRows (rows, onClick) {
      return rows.map((row, index) => ({
        ...row,
        onClick: () => onClick(index),
        style: {
          cursor: 'pointer'
        },
      }));
    };
  
      const headers = ["Topic Link"];
      const head = {
          cells: headers.map((headers) => ({
            key: headers,
            content: headers
          }))
        };
  
    function rowClick (para){
      setRowNumber(para);
    };

    return (
        <Fragment>
        <Wrapper>
          <h2>{heading}</h2>
          <h5 style={{ fontStyle: "italic", fontSize: "11px", color: "#9C9898", margin: "12px 2px", fontWeight: "400" }}>{subHeading}</h5>
          {(tagArray) &&
            tagArray.map((item, index) => {
              return(
                <Tag color="blueLight" text={item.label} key={index} />
              )
          })}
            <DynamicTable
                head={head}
                rows={loadingStateVal ? loading() : extendRows(rows.rows, rowClick)}
                rowsPerPage={10}
                highlightedRowIndex={rowNumber}
                defaultPage={1}
                emptyView={<EmptyTable />}
                loadingSpinnerSize="large"
                isLoading={false}
                isFixedSize
            />
        </Wrapper>
        </Fragment>

    );

}

export default Table;