import React, { useEffect, useState } from "react";
import { router } from "@forge/bridge";

export default function useCreateTable(data, setting){
    const [rows, setRows] = useState([]);

    function createTransData(data, setting) {
        // Reduce the data for table
        if((setting === null) || (setting.length === 0))
          setRows([]);
        else
        {
          let results = Object.entries(data).map(( [k,v] ) => ({ 
          sub: v.subject,
          key: v.id,
          link: v.view_href
        }));
    
          // Transform into data suitable for Table
          let Rows = [];
          let Tablerows = results.map((result,key) => ([{
              key: 1,
              content: <a onClick={() =>  router.open(result.link)}>{result.sub}</a>,
            }])
          );
          Rows.push(...Tablerows);
    
          // Transform into proper format for DynamicTable row
          let transformedRows = Rows.map((result, index) => ({
            key: index,
            cells: result,
          }));
    
          // Set the State to the transformed value
           setRows(transformedRows);
          }
    }
    
    useEffect(() => {
      createTransData(data,setting);
    },[data]);

    return{
        rows
    }
}