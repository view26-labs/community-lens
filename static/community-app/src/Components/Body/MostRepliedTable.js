import React, { useState } from "react";
import { useRecoilValue } from "recoil";
import SettingsAtom from "../../Recoil/SettingState";
import useCreateTable from "./useCreateTable";
import LoadingAtom from "../../Recoil/LoadingAtom";
import Table from "./Table";
import TableAtom from "../../Recoil/TableAtom";

const MostRepliedTable = () => {
  const replied = useRecoilValue(TableAtom);
  const settingState = useRecoilValue(SettingsAtom);
  const rows = useCreateTable(replied.Replies, settingState.replies)
  const [rowNumber, setRowNumber] = useState(0);
  const loadState = useRecoilValue(LoadingAtom);

  return (
    <Table heading="Topics with most replies" subHeading="track topics with most replies" tagArray={settingState.replies} rows={rows} loadingStateVal={loadState.isLoadingReplies} rowNumber={rowNumber} setRowNumber={setRowNumber} />
  );
}

export default MostRepliedTable;