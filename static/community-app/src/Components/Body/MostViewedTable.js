import React, { useState } from "react";
import { useRecoilValue } from "recoil";
import SettingsAtom from "../../Recoil/SettingState";
import useCreateTable from "./useCreateTable";
import LoadingAtom from "../../Recoil/LoadingAtom";
import Table from "./Table";
import TableAtom from "../../Recoil/TableAtom";

const MostViewedTable = () => {
  const views = useRecoilValue(TableAtom);
  const settingState = useRecoilValue(SettingsAtom);
  const loadState = useRecoilValue(LoadingAtom)
  const rows = useCreateTable(views.Views, settingState.views)
  const [rowNumber, setRowNumber] = useState(0);

  return (
      <Table heading="Most Viewed Topics" subHeading="track most viewed topics" tagArray={settingState.views} rows={rows} loadingStateVal={loadState.isLoadingViews} rowNumber={rowNumber} setRowNumber={setRowNumber} />
  );
}

export default MostViewedTable;