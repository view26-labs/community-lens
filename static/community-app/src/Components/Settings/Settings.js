import React, { useEffect, useState } from 'react'
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import Select from '@atlaskit/select';
import { invoke } from '@forge/bridge';
import { LabelSpan } from '../StyledComponents/Styling';
import classes from './Settings.module.css';
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil';
import SettingsAtom from '../../Recoil/SettingState';
import DateRangeAtom from '../../Recoil/DateRangeAtom';
import LoadingAtom from '../../Recoil/LoadingAtom';
import TableAtom from '../../Recoil/TableAtom';

const SettingsModal = ({ openModal, Changemodal }) => {
    const [topic, setTopic] = useState([]);
    const [board, setBoard] = useState([]);
    const [input, setInput] = useState('');
    const [labels, setLabels] = useState([]);
    const [labelsList] = useState([]);
    const [settingState, setSettingState] = useRecoilState(SettingsAtom);
    const dateRange = useRecoilValue(DateRangeAtom);
    const refInput = React.useRef(null);
    const setLoadState = useSetRecoilState(LoadingAtom);
    const setTableData = useSetRecoilState(TableAtom);

    useEffect(() => {
      getSettings();
    }, [dateRange]);

    useEffect(() => {
      setSettingState(prev => ({
        ...prev,
        mentions: labels
      }));
    },[labels]);

    async function getSettings() {
      console.log("inside getSettings function");
      await invoke('getSettings',).then( async (res) => {
        setSettingState(prev => ({
          ...prev,
          replies: res.settingState.replies,
          mentions: res.settingState.mentions,
          views: res.settingState.views
        }))
        let replied = [];
        replied = await invoke('mostReplied', dateRange);
        let viewed = [];
        viewed = await invoke('mostViewed', dateRange);  
        let mentioned = [];
        mentioned = await invoke('mentions', dateRange);
            setTableData(prev => ({
              ...prev,
              Mentions: mentioned,
              Views: viewed.data.items,
              Replies: replied.data.items
            }))
            setLoadState(prev => ({
              ...prev,
              isLoadingMentions: false,
              isLoadingViews: false,
              isLoadingReplies: false
            }))
    })
    .catch((error) => {
      console.log("Failed in getSettings", error);
    });
    setLabels(settingState.mentions);
    }

    function addLabel(label) {
            setInput('');
            setLabels([...settingState.mentions, {label: label}]);
            refInput.current.focus();
    }
  
    function removeAll() {
            setInput('');
            setLabels([]);
            refInput.current.focus();
    }
  
    function removeLabel(index) {
            setInput('');
            const newLabel = labels.filter((item, i) => i !== index);
            setLabels([...newLabel]);
            refInput.current.focus();
    }

    let labelListElements = [];
    let content = [];
    if (labelsList && labelsList.length) {
      labelListElements = labelsList.filter(item =>
        item.label.toLowerCase().includes(input.toLowerCase())
      );
    }
  
    if (labelListElements.length && input) {
      content = labelListElements.map((item, index) => {
        return (
          <li
            key={index}
            style={{ background: item.color }}
            onClick={() => addLabel(item.label)}
          >
            {item.label}
          </li>
        );
      });
    }
    if (input) {
      content.push(
        <li key={'add'} onClick={() => addLabel(input)}>
          Create new label: <strong>{input}</strong>
        </li>
      );
      content = <ul className={classes.list}>{content}</ul>;
    }
  
    //Options for Select component of Settings
    useEffect(() => {
        invoke('board',).then((res) => {
            console.log("BOARD response received",res);
            setTopic(res.data.items);
        });
    },[]);

    function createSelect(topic) {
        //Transform data to one acceptable by select component
        let result = topic.map( v => ({
            label: v.title,
            value: v.id
        }))
        setBoard(result);
    }

    useEffect(() => {
        createSelect(topic);
    },[topic]);
    
    return (
        <ModalTransition>
            {openModal && (
            <Modal actions={[
                { text: 'Cancel', appearance: "subtle", onClick: () => Changemodal(false) },
                { text: 'Submit', appearance: "primary", onClick: () => { 
                  invoke('setSettings', { settingState })
                  .then((res) => {
                    if(res){
                      getSettings();
                      setLoadState(prev => ({
                        ...prev,
                        isLoadingMentions: true,
                        isLoadingReplies: true,
                        isLoadingViews: true
                      }))
                    }
                    else
                      console.log("Failure in fetching data ");
                  })
                  Changemodal(false) }},
               ]}
            heading="App Settings"
            onClose={() => {
              Changemodal(false)
              }}>
                <form className="aui">
                    <div className="field-group">
                        <label htmlFor="Mentions">Mentions</label>
                        <br />
                        <div onClick={e => e.stopPropagation()}>
                            <div className={`${classes['tags-input']} flex w-full`}>
                                {settingState.mentions.map((label, index) => {
                                return (
                                    <LabelSpan
                                    key={index}
                                    className={`${classes.tag} max-w-full`}
                                    >
                                    {label.label}
                                    <span className={classes.close} onClick={() => removeLabel(index)} />
                                    </LabelSpan>
                                );
                                })}
                                <input
                                ref={refInput}
                                className={classes['main-input']}
                                value={input}
                                onChange={e => setInput(e.target.value)}
                                />
                                {settingState.mentions.length > 0 ? (
                                <span className={classes.closeAll} onClick={() => removeAll()}>
                                    ×
                                </span>
                                ) : null}
                            </div>
                            {content}
                        </div>
                    </div>
                    <br />
                      <div className={classes['field-group']}>
                        <label htmlFor="most-viewed-topics">Most viewed topics</label>
                        <br />
                        <Select
                            className="multi-select"
                            classNamePrefix="react-select"
                            onChange={(e) =>{ setSettingState(prev => ({
                              ...prev,
                              views: e
                            }));
                            console.log("e",e)}}
                            options={board}
                            value={settingState.views}
                            isMulti
                            isClearable
                            isSearchable={false}
                            placeholder="Choose a topic"
                        />
                    </div>
                    <br />
                    <div className={classes['field-group']}>
                        <label htmlFor="topics-with-most-replies">Topics with most replies</label>
                        <br />
                        <Select
                            className="multi-select"
                            classNamePrefix="react-select"
                            onChange={(e) =>{ setSettingState(prev => ({
                              ...prev,
                              replies: e
                            }));
                            console.log("e",e)}}
                            options={board}
                            value={settingState.replies}
                            isMulti
                            isClearable
                            isSearchable={false}
                            placeholder="Choose a topic"
                        />
                    </div>                    
                </form>
            </Modal>
            )}
        </ModalTransition>
    )
}

export default SettingsModal;
