import styled from "styled-components";

export const LabelComp = styled.div`
  align-self: center;
  color: #6b778c;
  font-size: 14px;
  padding: 12px 0;
`;

export const LabelSpan = styled.span.attrs({
  className: 'max-w-full  tags-input tag'
})`
  font-size: 85%;
  padding: 3px 7px;
  border-radius: 3px;
  margin: 5px 2px 0px 5px;
  display: inline-block;
  background-color: #ddd;
  transition: all 0.1s linear;
  cursor: pointer;
  overflow-wrap: anywhere;
`;