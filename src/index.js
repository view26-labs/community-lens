import Resolver from '@forge/resolver';
import api, { storage } from '@forge/api';

const BASE_API = 'https://community.atlassian.com/api/2.0/search?q='

const options = async() => {
  const query_API = 'SELECT id, title, view_href, boards FROM categories';
  const boards_API = BASE_API + encodeURIComponent(query_API);
  const fetchOptions = await api.fetch(`${boards_API}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: null
  });
  const opt = await fetchOptions.json();
  storage.set('boards',opt);
}

// LiQL query for getting topics of interest filtered by date range
// SELECT id, subject, view_href, replies.count(*) FROM messages WHERE subject MATCHES {tags} OR body MATCHES {tags} AND post_time > {updated_from} AND post_time < {updated_to} AND depth=0
const mentionsResponse = async(date, tags) => {
  const mentions_API = 'SELECT id, subject, view_href, replies.count(*) FROM messages WHERE subject MATCHES ' + `' " ${tags} " '` + ' OR body MATCHES ' + `' " ${tags} " '` + ' AND post_time > ' + `${date.UpdatedFrom}` + 'T10:04:30-08:00 AND post_time < ' + `${date.UpdatedTo}` + 'T10:04:30-08:00 AND depth=0';
  const boards_API = BASE_API + encodeURIComponent(mentions_API);
  const fetchOptions = await api.fetch(`${boards_API}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: null
  });
  return await fetchOptions.json();
}

// LiQL query for getting most viewed topics filtered by date range
// SELECT id, subject, view_href, metrics.views FROM messages WHERE post_time > {updated_from} AND post_time < {updated_to} AND tags.text in {tags} ORDER BY metrics.views DESC
const viewsResponse = async(date) => {
  const t = await storage.get('Settings');
  let tags = "";
  t.settingState.views === null ? tags = "[] " : (t.settingState.views.forEach(element => 
    tags = tags.concat(`"${element.value}",`)
  ))
  let text = tags.slice(0, tags.length-1);
  const views_API = 'SELECT id, subject, view_href, metrics.views FROM messages WHERE post_time > ' + `${date.UpdatedFrom}` + 'T10:04:30-08:00 AND post_time < ' + `${date.UpdatedTo}` + 'T10:04:30-08:00 AND tags.text in (' + `${text}` + ') ORDER BY metrics.views DESC';
  const boards_API = BASE_API + encodeURIComponent(views_API);
  const fetchOptions = await api.fetch(`${boards_API}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: null
  });
  return await fetchOptions.json();
}

// LiQL query for getting topics with most replies filtered by date range
// SELECT id, subject, view_href, replies.count(*) FROM messages WHERE post_time > {updated_from} AND post_time < {updated_to} AND tags.text in {tags} AND depth=0 ORDER BY replies.count(*) DESC LIMIT 10
const repliesResponse = async(date) => {
  const t = await storage.get('Settings');
  let tags = "";
  t.settingState.replies === null ? tags = "[] " : (t.settingState.replies.forEach(element => 
    tags = tags.concat(`"${element.value}",`)
  ))
  let text = tags.slice(0, tags.length-1);
  const replies_API = 'SELECT id, subject, view_href, replies.count(*) FROM messages WHERE post_time > ' + `${date.UpdatedFrom}` + 'T10:04:30-08:00 AND post_time < ' + `${date.UpdatedTo}` + 'T10:04:30-08:00 AND tags.text in (' + `${text}` + ') AND depth=0 ORDER BY replies.count(*) DESC LIMIT 10';
  const boards_API = BASE_API + encodeURIComponent(replies_API);
  const fetchOptions = await api.fetch(`${boards_API}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: null
  });
  return await fetchOptions.json();
}

const resolver = new Resolver();

resolver.define('board', async () => {
  const res = await options();
  return await storage.get('boards');
});

resolver.define('setSettings', async ({payload}) => {
  console.log("entering set settings ..");
  storage.set('Settings', payload);
});

resolver.define('getSettings', async () => {
  const t = await storage.get('Settings');
  if(!t){
    let settingState = {
      mentions: [],
      replies: [{label: "Jira", value: "jira"}],
      views: [{label: "Jira", value: "jira"}],
    }
    storage.set('Settings', {settingState} );
    return {settingState};
  }
  else
    return t;
});

resolver.define('mostReplied', async ({payload}) => {
  const res = await repliesResponse(payload);
  return res;
});

resolver.define('mostViewed', async ({payload}) => {
  const res = await viewsResponse(payload);
  return res;
});

resolver.define('mentions', async ({payload}) => {
  const t = await storage.get('Settings');
  let res = await Promise.all(t.settingState.mentions.map(element => mentionsResponse(payload, element.label)))
  let arr = [];
  res.forEach(element =>
    arr = arr.concat(...element.data.items));
  return arr;
});

export const handler = resolver.getDefinitions();